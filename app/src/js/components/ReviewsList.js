import React from "react";

function getReviewsInReverseOrder(reviews) {
  if(reviews.length === 0) {
    return;
  }

  return (
    reviews.slice(0).reverse().map((item) => {
      return (
        <div key={item.id} className="pt-3 mt-3 border-top">
          <div className="text-left">{ item.review }</div>
          <div className="text-right text-muted mt-3">autor: { item.author }</div>
        </div>
      )
    })
  )
}

function ReviewsList(props) {
  return (
    <div className="bg-white border p-4 mb-4">
      <div className="font-weight-bold text-left lead mb-2">
        <span>Recenzje</span>
        <span className="ml-2 text-muted">({props.reviews.length})</span>
      </div>
      {getReviewsInReverseOrder(props.reviews)}
    </div>
  );
}

export default ReviewsList;
