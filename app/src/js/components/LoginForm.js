import React, {useState} from "react";
import Cookies from 'js-cookie'

const LoginForm = (props) => {
    const [fields, setFields] = useState({ email: '', password: '' });
    const [formState, setFormState] = useState({responseStatus: null, loading: false });
    const [isValidationError, setIsValidationError] = useState(null);

    const login  = async () => {
        props.setIsModalLoading(true);
        let newFormState = {...formState};

        for(let field of Object.values(fields)) {
            if (field.length === 0) {
                setIsValidationError(true);
                newFormState.responseStatus = false;
                setFormState(newFormState);
                props.setIsModalLoading(false);
                return;
            }
        }

        newFormState.loading = true;
        setFormState(newFormState);

        const response = await fetch(
            process.env.REACT_APP_LOGIN_API_URL,
        {
              method: 'POST',
              headers: {'Accept': 'application/json', 'Content-Type': 'application/json',},
              body: JSON.stringify({
                email: fields.email,
                password: fields.password
              })
            }
        );

        props.setIsModalLoading(false);

        if (!response.ok) {
            return setIsValidationError(true);
        }

        response.json().then(data => {
            Cookies.set('user', {
                email: data.email,
                name: data.name
            });
        });

        setIsValidationError(false);

        newFormState = {
            responseStatus: true,
            loading: false
        };

        setFields({email: fields.email, password: ''});
        setFormState(newFormState);
        window.location.reload();
    };

    const handlePasswordChange = (event) => {
        let newFields = {...fields};
        newFields.password = event.target.value;
        setFields(newFields);
    };

    const handleEmailChange = (event) => {
        let newFields = {...fields};
        newFields.email = event.target.value;
        setFields(newFields);
    };

    return (
        <div className="login-form">
            <h2 className="mb-4">Zaloguj się</h2>
            <input
              type="text"
              className="auth-input"
              placeholder="Email"
              value={fields.email || ''}
              onChange={handleEmailChange}
            />
            <input
              type="password"
              className="auth-input"
              placeholder="Hasło"
              value={fields.password || ''}
              onChange={handlePasswordChange}
            />
            <button onClick={() => {login()}} className="btn btn-primary px-4 py-2 text-left">Zaloguj się</button>
            { isValidationError &&
                <div className="alert-danger mt-3 p-2">Błędne dane, sprobuj jeszcze raz.</div>
            }
            { props.isRegistered &&
                <div className="alert-success mt-3 p-2">Rejestracja przebiegła przemyślnie. Możesz się zalogować.</div>
            }
        </div>
    );
};

export default LoginForm;
