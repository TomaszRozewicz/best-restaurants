import React from "react";

const Intro = (props) => {
    return (
      <div className="intro__background" style={{backgroundImage: `url(${props.restaurant.image})`}}>
        <h1 className="intro__title">{ props.restaurant.name }</h1>
      </div>
    );
};

export default Intro;