import React, {useState} from "react";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
import AuthLoading from "./AuthLoading";
import Cookies from "js-cookie";

const AuthModal = (props) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isRegistered, setIsRegistered] = useState(false);

    const toggleLoadingState = (newState) => {
        setIsLoading(newState);
    };

    if (!props.isVisible || Cookies.get('user')) {
        return '';
    }

    const onRegister = () => {
        props.toggleModalContent();
        console.log(123)
        setIsRegistered(true);
    };

    return (
        <div className="auth-modal">
            <div className="auth-modal__wrapper">
                <AuthLoading isVisible={isLoading}/>
                <div className={`${isLoading ? 'd-none' : ''}`}>
                    <div className="auth-modal__close" onClick={() => {props.toggleModal()}}>x</div>
                    <div className="mb-4 pb-4 border-bottom">
                        { props.isLoginForm ?
                            <LoginForm
                                toggleModal={props.toggleModal}
                                setIsModalLoading={toggleLoadingState}
                                isRegistered={isRegistered}
                            /> :
                            <RegisterForm
                                toggleModal={props.toggleModal}
                                setIsModalLoading={toggleLoadingState}
                                onRegister={onRegister}
                            />
                        }
                    </div>
                    <div className="text-muted mb-3">lub</div>
                    <div onClick={props.toggleModalContent} className="btn btn-primary px-4 py-2">
                        { props.isLoginForm ?
                            <span>Zarejestruj się</span>
                        :
                            <span>Zaloguj się</span>
                        }
                    </div>
                </div>
            </div>
            <div className="auth-modal__background" onClick={() => {props.toggleModal()}}/>
        </div>
    );
};

export default AuthModal;
