import React from "react";

const AuthLoading = (props) => {
  return (
    <div className={`loading-ring loading-ring--center ${!props.isVisible ? 'd-none' : ''}`}>
      <div className="loading-ring__inner loading-ring__inner--big">
        <div/>
        <div/>
        <div/>
        <div/>
      </div>
    </div>
  );
};

export default AuthLoading;
