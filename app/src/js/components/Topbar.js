import React from "react";
import Cookies from "js-cookie";
const Topbar = (props) => {
    const userName = Cookies.get('user') ? JSON.parse(Cookies.get('user')).name : null;
    const logout = () => {
        Cookies.remove('user');
        window.location.reload();
    };
    return (
        <div className="topbar">
            {Cookies.get('user') ?
                <div className="d-flex justify-content-end">
                    <div className="mx-2">użytkownik {userName}</div>
                    <div onClick={logout} className="mx-2 cursor-pointer">Wyloguj się</div>
                </div>
                :
                <div onClick={props.onLogin} className="mx-2 cursor-pointer">Zaloguj się / Zarejestruj się</div>
            }
        </div>
    );
};

export default Topbar;
