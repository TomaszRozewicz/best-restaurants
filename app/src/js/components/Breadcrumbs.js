import React from "react";

const Breadcrumbs = (props) => {
  return (
    <div>
      <nav aria-label="breadcrumb">
        <ol className="breadcrumb bg-transparent px-0 mt-3 mb-2">
          <li className="breadcrumb-item pr-2"><a href={process.env.REACT_APP_RESTAURANTS_APP}>Home</a></li>
          <li className="breadcrumb-item active pl-0" aria-current="page">{ props.restaurantName }</li>
        </ol>
      </nav>
    </div>
  );
};

export default Breadcrumbs;