import React from "react";

const FormLoading = () => {
  return (
    <div className="d-flex align-items-center mt-3 mt-md-0 ml-0 ml-md-3 justify-content-center justify-content-md-start">
      <div className="loading-ring">
        <div/>
        <div/>
        <div/>
        <div/>
      </div>
    </div>
  );
};

export default FormLoading;