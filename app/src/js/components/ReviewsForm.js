import React, {useState} from "react";
import FormResponse from "./FormResponse"
import Cookies from "js-cookie";

const ReviewsForm = (props) => {
  const userName = Cookies.get('user') ? JSON.parse(Cookies.get('user')).name : null;
  const [fields, setFields] = useState({ author: userName ? userName : '', review: '' });
  const [formState, setFormState] = useState({responseStatus: null, loading: false });

  const sendReview = async () => {
    let newFormState = {...formState};

    for(let field of Object.values(fields)) {
      if(field.length === 0) {
        newFormState.responseStatus = false;
        setFormState(newFormState);
        return;
      }
    }

    newFormState.loading = true;
    setFormState(newFormState);

    const response = await fetch(process.env.REACT_APP_REVIEWS_API_URL, {method: 'POST', headers: {'Accept': 'application/json', 'Content-Type': 'application/json',}, body: JSON.stringify({ author: fields.author, review: fields.review, restaurants_id: props.restaurant.id })});

    if (!response.ok) {
      console.log();
      throw Error(response.statusText);
    }


    newFormState = {
      responseStatus: true,
      loading: false
    };

    setFields({author: userName ? userName : '', review: ''});
    setFormState(newFormState);

    props.onFormChange({id: props.restaurant.id});
  };

  const handleReviewChange = (event) => {
    let newFields = {...fields};
    newFields.review = event.target.value;
    setFields(newFields);
  };

  const handleAuthorChange = (event) => {
    let newFields = {...fields};
    newFields.author = event.target.value;
    setFields(newFields);
  };

  return (
    <div className="mb-5 mx-auto reviews-form">
      <div className="font-weight-bold text-left lead mb-2">Dodaj swoją opinię</div>
      <div>
          <textarea
            name=""
            id=""
            cols="30"
            rows="3"
            placeholder="Opisz swoje wrażenia: posiłki, atmosfera, obsługa?"
            className="w-100 d-block mb-3 list-item__review"
            value={fields.review || ''}
            onChange={handleReviewChange}
          />
        { userName ?
          <div className="text-left">Zalogowany jako {userName}</div> :
          <input
            type="text"
            className="w-100 d-block mb-3 list-item__author"
            placeholder="Twoje imię i nazwisko..."
            value={fields.author || ''}
            onChange={handleAuthorChange}
          />
        }
        <div className="g-recaptcha" data-sitekey="6Ler0swUAAAAADypLXuiuGfwdO54FFhV0oIaRziu"></div>
        <div className="d-md-flex mt-3">
          <button onClick={() => {sendReview()}} className="btn btn-primary px-4 py-2 text-left">Prześlij recenzję</button>
          <FormResponse loading={formState.loading} responseStatus={formState.responseStatus}/>
        </div>
      </div>
    </div>
  );
};

export default ReviewsForm
