import {NavLink} from "react-router-dom";
import React from "react";

function truncateDescripton(description) {
  let lengthTarget = 90;
  if(description.length > lengthTarget) {
    return `${description.substring(0, lengthTarget)}...`;
  }
  return description;
}

const ListItem = (props) => {
    let backgroundImage = `url(${ props.data.image })`;

    return (
      <NavLink to={`/restaurant/${ props.data.id }`} className="list-item">
        <div>
          <div className="list-item__image-container">
            <div className="border-right list-item__image" style={{ backgroundImage: backgroundImage }}/>
            <i className="material-icons list-item__icon-photo">
              insert_photo
            </i>
          </div>
        </div>

        <div className="m-3 m-md-4">
          <h3 className="list-item__title">{ props.data.name }</h3>
          <div className="list-item__location">
            <span>Lokalizacja: </span>
            <span>{ props.data.location }</span>
        </div>
          <div className="text-left d-none d-md-block">{ truncateDescripton(props.data.description) }</div>
        </div>
      </NavLink>
    );
};

export default ListItem;