import FormLoading from "./FormLoading";
import React from "react";

const FormResponseStatement = (props) => {
  return (
    <div className={`${props.class} d-flex align-items-center mt-3 mt-md-0 ml-0 ml-md-3 justify-content-center justify-content-md-start`}>
      {props.text}
    </div>
  )
};

const FormResponse = (props) => {
  if(props.loading) {
    return <FormLoading/>
  }

  if(!props.loading && props.responseStatus === true) {
    return <FormResponseStatement class={"text-success"} text={"Opinia wysłana. Dziękujemy"}/>;
  }

  if(!props.loading && props.responseStatus === false) {
    return <FormResponseStatement class={"text-danger"} text={"Wypełnij puste pola"}/>
  }

  return <div/>;
};

export default FormResponse