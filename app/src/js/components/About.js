import React from "react";

const About = (props) => {
  return (
    <div className="mb-5 p-4 bg-white border">
      <div className="font-weight-bold text-left lead mb-2">O restauracji</div>
      <div className="text-left text-muted mb-1">
        <span>Lokalizacja: </span>
        <span>{ props.restaurant.location }</span>
      </div>
      <div className="text-left">{ props.restaurant.description }</div>
    </div>
  );
};

export default About;