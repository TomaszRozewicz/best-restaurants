import React, {useState} from "react";

const RegisterForm = (props) => {
    const [fields, setFields] = useState({ email: '', nick: '', password: '' });
    const [formState, setFormState] = useState({responseStatus: null, loading: false });
    const [isValidationError, setIsValidationError] = useState(null);

    const register  = async () => {
        props.setIsModalLoading(true);
        let newFormState = {...formState};

        for(let field of Object.values(fields)) {
            if (field.length === 0) {
                setIsValidationError(true);
                newFormState.responseStatus = false;
                setFormState(newFormState);
                props.setIsModalLoading(false);
                return;
            }
        }

        newFormState.loading = true;
        setFormState(newFormState);

        const response = await fetch(
            process.env.REACT_APP_REGISTER_API_URL,
            {
                method: 'POST',
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json',},
                body: JSON.stringify({
                    email: fields.email,
                    name: fields.nick,
                    password: fields.password
                })
            }
        );

        props.setIsModalLoading(false);

        if (!response.ok) {
            return setIsValidationError(true);
        }

        setIsValidationError(false);

        newFormState = {
            responseStatus: true,
            loading: false
        };

        setFields({email: fields.email, nick: fields.nick, password: ''});
        props.onRegister();
        setFormState(newFormState);
    };

    const handlePasswordChange = (event) => {
        let newFields = {...fields};
        newFields.password = event.target.value;
        setFields(newFields);
    };

    const handleEmailChange = (event) => {
        let newFields = {...fields};
        newFields.email = event.target.value;
        setFields(newFields);
    };

    const handleNickChange = (event) => {
        let newFields = {...fields};
        newFields.nick = event.target.value;
        setFields(newFields);
    };

    return (
        <div className="register-form">
            <h2 className="mb-4">Zarejestruj się</h2>
            <input
                type="email"
                className="auth-input"
                placeholder="Email"
                value={fields.email || ''}
                onChange={handleEmailChange}
            />
            <input
                type="text"
                className="auth-input"
                placeholder="Nick"
                value={fields.nick || ''}
                onChange={handleNickChange}
            />
            <input
                type="password"
                className="auth-input"
                placeholder="Hasło"
                value={fields.password || ''}
                onChange={handlePasswordChange}
            />
            <button onClick={() => {register()}} className="btn btn-primary px-4 py-2 text-left">Zarejestruj się</button>
            { isValidationError &&
                <div className="alert-danger mt-3 p-2">Błędne dane, sprobuj jeszcze raz.</div>
            }
        </div>
    );
};

export default RegisterForm;
