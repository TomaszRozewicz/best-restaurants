import React, {useState} from 'react';
import { Route, HashRouter, Switch } from "react-router-dom";
import Index from "./pages/Index";
import Restaurant from "./pages/Restaurant";
import NotFound from "./pages/NotFound";

import AuthModal from "./components/AuthModal";
import Topbar from "./components/Topbar";

const App = () => {
  const [authModalVisibility, setAuthModalVisibility] = useState(false);
  const [loginFormVisibility, setLoginFormVisibility] = useState(true);

  const toggleModal = () => {
    setAuthModalVisibility(!authModalVisibility)
  };

  const toggleModalContent = () => {
    setLoginFormVisibility(!loginFormVisibility)
  };

  return (
    <HashRouter>
      <div className="App">
        <div className="content">
          <AuthModal
            toggleModal={toggleModal}
            isVisible={authModalVisibility}
            isLoginForm={loginFormVisibility}
            toggleModalContent={toggleModalContent}
          />
          <Topbar onLogin={toggleModal}/>
          <Switch>
            <Route exact path="/" component={Index}/>
            <Route path="/restaurant/:id" component={Restaurant}/>
            <Route path="/not-found" component={NotFound}/>
          </Switch>
        </div>
      </div>
    </HashRouter>
  );
}

export default App;
