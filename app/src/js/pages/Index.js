import React, { useState, useEffect } from "react";
import Background from '../../img/main-background.jpg';
import ListItem from "../components/ListItem"

const Index = () => {
  const [restaurants, setRestaurants] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(process.env.REACT_APP_RESTAURANTS_API_URL, {method: 'GET', mode: 'cors'})

      if (!response.ok) {
        throw Error(response.statusText);
      }

      const restaurants = await response.json();
      setRestaurants(restaurants);
    };

    fetchData();
  }, []);

  const data = restaurants.map((item) =>
    <ListItem data={item} key={item.id}/>
  );

  return (
    <div>
      <div className="intro__background" style={{backgroundImage: `url(${Background})`}}>
        <h1 className="intro__title">Wyszukuj najlepsze restauracje w twojej okolicy</h1>
      </div>
      <div className="container mb-5">
        <h2 className="mt-5 mb-4">Restauracje</h2>
        {data}
      </div>
    </div>
  );
};

export default Index;
