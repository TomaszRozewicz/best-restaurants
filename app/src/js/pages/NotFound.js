import React from "react";
import {NavLink} from "react-router-dom";

const NotFound = () => (
  <div className="not-found">
    <div>
      <div>ooops, something went wrong</div>
      <NavLink to={`/#/`} className="btn btn-light">Go To Home</NavLink>
    </div>
  </div>
);

export default NotFound;