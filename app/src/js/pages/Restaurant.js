import React, {useEffect, useState} from "react";
import Intro from "../components/Intro"
import Breadcrumbs from "../components/Breadcrumbs"
import About from "../components/About"
import ReviewsForm from "../components/ReviewsForm"
import ReviewsList from "../components/ReviewsList"

const Restaurant = (props) => {
  const [restaurant, setRestaurant] = useState([]);
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const id = props.match.params.id;

      await getRestaurant({id});
      await getReviews({id});
    };

    getData();
  }, [props]);


  const getRestaurant = async (props) => {
    const response = await fetch(`${process.env.REACT_APP_RESTAURANTS_API_URL}/${props.id}`, {method: 'GET', mode: 'cors'});

    if (!response.ok) {
      throw Error(response.statusText);
    }

    const restaurants = await response.json();
    setRestaurant(restaurants);
  };

  const getReviews = async (props) => {
    const response = await fetch(`${process.env.REACT_APP_REVIEWS_API_URL}/restaurantId/${props.id}`, {method: 'GET', mode: 'cors'})

    if (!response.ok) {
      throw Error(response.statusText);
    }

    const reviews = Object.values(await response.json());
    setReviews(reviews);
  };

  return (
    <div>
      <Intro restaurant={ restaurant }/>
      <div className="restaurant mx-auto px-3">
        <Breadcrumbs restaurantName={ restaurant.name }/>
        <About restaurant={ restaurant }/>
        <ReviewsForm restaurant={restaurant} onFormChange={getReviews}/>
        <ReviewsList reviews={reviews}/>
      </div>
    </div>
  );
};

export default Restaurant;