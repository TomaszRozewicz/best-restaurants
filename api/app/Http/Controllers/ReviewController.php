<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;

class ReviewController extends Controller
{
  public function index()
  {
    return Review::all();
  }

  public function indexByRestaurantId($restaurantId)
  {
    return Review::all()->where('restaurants_id', $restaurantId);
  }

  public function show(Review $restaurant)
  {
    return $restaurant;
  }

  public function store(Request $request)
  {
    $restaurant= Review::create($request->all());

    return response()->json($restaurant, 201);
  }

  public function update(Request $request, Review $restaurant)
  {
    $restaurant->update($request->all());

    return response()->json($restaurant, 200);
  }

  public function delete(Review $restaurant)
  {
    $restaurant->delete();

    return response()->json(null, 204);
  }
}
