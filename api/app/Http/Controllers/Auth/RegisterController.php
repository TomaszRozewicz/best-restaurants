<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:email'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {
        $customMessages = [
          'required' => 'Wpisz :attribute.',
          'name.required' => 'Wpisz nick.',
          'email' => 'Wpisz poprawny :attribute.',
          'email.unique' => 'Adres email już istnieje.',
          'password.min' => 'Hasło musi mieć minimum 6 znaków.'
        ];

        $validator = Validator::make($data->all(), [
          'name' => 'required',
          'email' => 'required|email|unique:users',
          'password' => 'required|min:6'
        ], $customMessages);

         if ($validator->fails()) {
           return response()->json(['error'=>$validator->errors()], 401);
         }

         $input = $data->all();
         $input['password'] = bcrypt($input['password']);
         $user = User::create($input);
         return response()->json(200);
    }
}
