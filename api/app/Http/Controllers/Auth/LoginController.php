<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function index(Request $data)
    {
    if(Auth::attempt(['email' => $data['email'], 'password' => $data['password']])){
       $user = Auth::user();
       $success['token'] =  'success';
        return response()->json([
            'success' => $success,
            'email' => $user['email'],
            'name' => $user['name']
        ], 200);
      } else{
       return response()->json(['error'=>'Unauthorised'], 401);
       }
    }
}
