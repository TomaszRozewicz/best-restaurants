<?php

use Illuminate\Database\Seeder;
use App\Review;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Review::truncate();

      $faker = \Faker\Factory::create();

      for ($i = 0; $i < 100; $i++) {
        Review::create([
          'author' =>$faker->name,
          'review' =>$faker->sentence($nbWords = 30, $variableNbWords = true),
          'restaurants_id' =>$faker->numberBetween($min = 1, $max = 10),
        ]);
      }
    }
}
