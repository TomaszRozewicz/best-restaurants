<?php

use Illuminate\Database\Seeder;
use App\Restaurant;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Restaurant::truncate();

      $faker = \Faker\Factory::create();

      for ($i = 0; $i < 10; $i++) {
        Restaurant::create([
          'name' => $faker->company,
          'description' => $faker->sentence($nbWords = 30, $variableNbWords = true),
          'location' => $faker->streetName,
          'image' => $faker->imageUrl($width = 640, $height = 480, 'food'),
        ]);
      }
    }
}
