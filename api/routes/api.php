<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('restaurants', 'RestaurantController@index');
Route::get('restaurants/{restaurant}', 'RestaurantController@show');
//Route::post('restaurants', 'RestaurantController@store');
//Route::put('restaurants/{restaurant}', 'RestaurantController@update');
//Route::delete('restaurants/{restaurant}', 'RestaurantController@delete');

Route::get('reviews', 'ReviewController@index');
Route::get('reviews/restaurantId/{restaurantsId}', 'ReviewController@indexByRestaurantId');
Route::get('reviews/{review}', 'ReviewController@show');
Route::post('reviews', 'ReviewController@store');
//Route::put('reviews/{review}', 'ReviewController@update');
//Route::delete('reviews/{review}', 'ReviewController@delete');

Route::get('users', 'UserController@index');
Route::post('register', 'Auth\RegisterController@create');
Route::post('login', 'Auth\LoginController@index');
